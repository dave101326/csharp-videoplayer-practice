﻿using LibVLCSharp.Shared;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Windows.Threading;

namespace VideoPlayer
{
    public partial class Form1 : Form
    {
        private LibVLC libVLC;
        private MediaPlayer mp;
        private LibVLCSharp.WinForms.VideoView videoview1;

        private DispatcherTimer videotimer;
        private Media media;
        private OpenFileDialog dlg;

        public Form1()
        {
            InitializeComponent();
            this.Icon = null;
            Core.Initialize();
            videotimer = new DispatcherTimer();
            videotimer.Interval = TimeSpan.FromSeconds(0.1);
            videotimer.Tick += new EventHandler(timer_Tick);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ToolTip tooltip = new ToolTip();
            tooltip.SetToolTip(playBtn, "Play");
            tooltip.SetToolTip(pauseBtn, "Pause");
            tooltip.SetToolTip(stopBtn, "Stop");
            tooltip.SetToolTip(button1, "Open Media");
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                trackBar1.Value = (int)(mp.Position * 1000);
            }
            catch
            {

            }
        }

        private void playBtn_Click(object sender, EventArgs e)
        {
            if (mp != null)
            {
                mp.Play();
                EnableButton(true);
            }
        }

        private void pauseBtn_Click(object sender, EventArgs e)
        {
            if (mp != null)
            {
                mp.Pause();
                EnableButton(false);
            }
        }

        private void stopBtn_Click(object sender, EventArgs e)
        {
            if (mp != null)
            {
                trackBar1.Value = (int)(0 * 1000);
                mp.Stop();
                EnableButton(false);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dlg = new OpenFileDialog();
            dlg.Filter = "wmv file(*.wmv)|*.wmv";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                MediaPlay();
            }
        }

        private void EnableButton(bool isPlay)
        {
            videotimer.IsEnabled = isPlay;
        }

        private void MediaPlay()
        {
            if (!File.Exists(dlg.FileName))
            {
                MessageBox.Show("File not found", "XXX", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] bytesFile = File.ReadAllBytes(dlg.FileName);

            MemoryStream ms = new MemoryStream();

            if (bytesFile == null)
            {
                MessageBox.Show("File format error", "XXX", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            ms.Write(bytesFile, 0, bytesFile.Length);

            if (ms.Length > 0)
            {
                libVLC = new LibVLC();
                mp = new MediaPlayer(libVLC);
                videoview1 = new LibVLCSharp.WinForms.VideoView();
                videoview1.MediaPlayer = mp;
                mp.SetMarqueeInt(VideoMarqueeOption.Enable, 1);
                mp.SetMarqueeInt(VideoMarqueeOption.Size, 120);
                mp.SetMarqueeInt(VideoMarqueeOption.Position, 0);
                mp.SetMarqueeString(VideoMarqueeOption.Text, Path.GetFullPath(dlg.FileName));
                mp.SetMarqueeInt(VideoMarqueeOption.Opacity, 40);
                mp.SetMarqueeInt(VideoMarqueeOption.Color, 0xff3300);

                mp.Fullscreen = true;
                media = new Media(libVLC, new StreamMediaInput(ms));
                this.Controls.Add(videoview1);

                videoview1.Parent = panel1;
                videoview1.Location = new Point(0, 0);
                videoview1.Width = panel1.Width;
                videoview1.Height = panel1.Height;

                trackBar1.Minimum = 0;
                trackBar1.Maximum = 1000;
                EnableButton(true);
                mp.Play(media);
            }
            else
            {
                MessageBox.Show("File format error", "XXX", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (videoview1 != null && Form1.ActiveForm != null)
            {
                videoview1.Width = panel1.Width;
                videoview1.Height = panel1.Height;
            }
        }
    }
}
